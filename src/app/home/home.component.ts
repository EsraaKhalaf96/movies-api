import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../movies.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
 trendingMovies:any[]=[];
 imgPrefix:string ='https://image.tmdb.org/t/p/w500/';
  constructor(private _moviesService:MoviesService) {
    _moviesService.getTrending('movie').subscribe((data)=>{
      this.trendingMovies = data.results;
    })
   }
  ngOnInit(): void {
  }

}
