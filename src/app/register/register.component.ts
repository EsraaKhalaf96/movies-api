import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
   registerForm = new FormGroup ({
    first_name : new FormControl(null,[Validators.minLength(3),Validators.maxLength(10),Validators.required]),
    last_name : new FormControl(null,[Validators.minLength(3),Validators.maxLength(10),Validators.required]),
    email : new FormControl(null,[Validators.required,Validators.email]),
    password : new FormControl(null,[Validators.required,Validators.pattern('^[A-Z][a-z0-9]{3,8}$')]),
    age : new FormControl(null,[Validators.required,Validators.min(16),Validators.max(80)]),
    
   });

  constructor() { }

  ngOnInit(): void {
  }
  onSubmit(){
    console.log(this.registerForm)
  }

}
